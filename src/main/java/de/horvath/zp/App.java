package de.horvath.zp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Main application. Loads and interprets its input data, calculates the output and shows the result.
 * <p>
 * Intended to be called from command line jre (java.exe).
 */
public class App {
	private static int[][] pyramid;
	private static int height;

	private static int[][] parents;
	private static int[][] pathLength;

	private static int[] intLineSplit(String line) {
		String[] rawTerms = line.split(" ");
		LinkedList<Integer> rawInt = new LinkedList<Integer>();
		for (String term : rawTerms) {
			if (term.equals(""))
				continue;
			try {
				rawInt.add(Integer.parseInt(term));
			} catch (NumberFormatException e) {
				continue;
			}
		}
		int n=0;
		int[] result = new int[rawInt.size()];
		for (Integer i : rawInt)
			result[n++] = i;

		/*
		System.out.println("line: "+line);
		System.out.print("result:");
        for (int i : result)
			System.out.print(" "+i);
		System.out.println(".");
		*/

		return result;
	}

	private static void dumpTriangle(int[][] t) {
		for (int n=0; n<height; n++) {
			System.out.print("line "+n+": ");
			for (int m=0; m<=n; m++) {
				if (m>0)
					System.out.print(", ");
				System.out.print(t[n][m]);
			}
			System.out.println(".");
		}
	}

	private static void readInput(String fileName) {
		List<String> lines;
		try {
			lines = Files.readAllLines(Paths.get(fileName));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		height = lines.size();
		pyramid = new int[height][height];
		int n=0;
		for (String line : lines)
			pyramid[n++] = intLineSplit(line);

		//System.out.println(lines.toString());
		//System.out.println(pyramid.toString());

		//dumpTriangle(pyramid);
	}

	private static void leftParent(int n, int m) {
		parents[n][m] = m-1;
		pathLength[n][m] = pathLength[n-1][m-1] + pyramid[n][m];
	}

	private static void rightParent(int n, int m) {
		parents[n][m] = m;
		pathLength[n][m] = pathLength[n-1][m] + pyramid[n][m];
	}

	private static void calcMaxPath() {
		parents = new int[height][height];
		pathLength = new int[height][height];

		pathLength[0][0] = pyramid[0][0];
		parents[0][0] = -1;

		for (int n=1; n<height; n++) {
			rightParent(n, 0);
			for (int m=1; m<n; m++) {
				if (pathLength[n-1][m-1]>pathLength[n-1][m])
					leftParent(n, m);
				else
					rightParent(n, m);
			}
			leftParent(n, n);
		}
	}

	private static void showResult() {
		// find maximal
		//dumpTriangle(parents);
		//dumpTriangle(pathLength);
		int maxPathLength=0;
		int maxPathPos=-1;
		for (int n=0; n<height; n++)
			if (pathLength[height-1][n]>maxPathLength) {
				maxPathPos = n;
				maxPathLength = pathLength[height-1][n];
			}

		System.out.println("Maximal path: "+maxPathLength);
		int curPos = maxPathPos;
		for (int n=height-1; n>=0; n--) {
			System.out.println("line "+n+": "+pyramid[n][curPos]);
			curPos = parents[n][curPos];
		}
	}

	/**
 	* @param  args Only a single argument is allowed, which contains the name of the input file.
	*/
    public static void main( String[] args ) {
		readInput(args[0]);
		calcMaxPath();
		showResult();
    }
}
